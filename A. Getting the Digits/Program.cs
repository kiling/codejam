﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CodeJam;

namespace A.Getting_the_Digits
{
    internal class Problem : Helper
    {
        protected string DeleteSubString(string original, string subString)
        {
            Regex reg = new Regex(subString);
            return reg.Replace(original, "", 1);
        }

        protected override string Calculate(string input)
        {
            var digits = new string[]
            {
                "ZERO", "ONE", "TWO", "THREE",
                "FOUR", "FIVE", "SIX", "SEVEN",
                "EIGHT", "NINE"
            };

            var resultDigits = new List<int>();
            input = "TSNEIISTOGXEVHEW";


            while (input.Count() > 0)
            {
                var newDigits = digits;

                foreach (var chr in input)
                {
                    for (int k = 0; k < newDigits.Count(); k++)
                    {
                        var digit = newDigits[k];
                        if (digit.Contains(chr))
                        {
                            digit = digit.Replace(chr.ToString(), "");
                            if (digit.Length == 0)
                            {
                                resultDigits.Add(k);

                                foreach (var d in digits[k])
                                    input = DeleteSubString(input, d.ToString());
                            }
                        }
                    }
                }
            }
            

            var result = "";
            resultDigits.Sort();
            foreach (var dg in resultDigits)
                result += dg.ToString();

            return result;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var problem = new Problem();
            problem.Load();
            problem.Start();
            problem.Save();
            Console.ReadLine();
        }
    }
}
