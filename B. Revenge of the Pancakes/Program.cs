﻿using System;
using System.Collections.Generic;
using System.Linq;
using CodeJam;

namespace B.Revenge_of_the_Pancakes
{
    class Problem : Helper
    {
        protected override string Calculate(string input)
        {
            var pancakes = new List<char>();
            for (var i = 0; i < input.Length; i++)
                pancakes.Add(GetValueChar(input, i));

            var count = 0;
            
            while (true)
            {
                var result = true;
                foreach (var p in pancakes)
                {
                    if (p == '-')
                    {
                        result = false;
                        break;
                    }
                }

                if (result)
                    break;

                var lastPancakes = pancakes.Count - 1;
                var lastStatus = pancakes.Last();

                for (int i = pancakes.Count - 1; i >= 0; i--)
                {
                    if (pancakes[i] != lastStatus)
                    {
                        lastPancakes = i;
                        break;
                    }
                }

                for (int i = 0; i <= lastPancakes; i++)
                {
                    if (pancakes[i] == '-')
                        pancakes[i] = '+';
                    else
                    if (pancakes[i] == '+')
                        pancakes[i] = '-';
                }

                count++;
            }

            return count.ToString();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var problem = new Problem();
            problem.Load();
            problem.Start();
            problem.Save();
            Console.ReadLine();
        }
    }
}
