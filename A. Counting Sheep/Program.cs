﻿using System;
using System.Collections.Generic;
using CodeJam;

namespace A.Counting_Sheep
{
    class Problem: Helper
    {
        protected override string Calculate(string input)
        {
            var value = GetValueInt64(input, 0);

            if (value == 0)
                return "INSOMNIA";

            var digits = new List<int>()
            {
                0, 1, 2, 3, 4, 5, 6, 7, 8, 9
            };

            Int64 n = 1;
            while (true)
            {
                Int64 currentValue = n*value;

                var str = currentValue.ToString();
                foreach (var d in str)
                {
                    var item = (int)Char.GetNumericValue(d);
                    digits.Remove(item);
                }

                if (digits.Count == 0)
                {
                    return currentValue.ToString();
                }

                n++;
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var problem = new Problem();
            problem.Load();
            problem.Start();
            problem.Save();
            Console.ReadLine();
        }
    }
}
