﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeJam
{
    public abstract class Helper
    {
        protected readonly List<string> _inputs = new List<string>();
        protected List<string> _outputs = new List<string>(); 
        private int _countInput = 0;

        public void Load()
        {
            _inputs.Clear();

            string line;
            var file = new System.IO.StreamReader("input.in");

            _countInput = Convert.ToInt32(file.ReadLine());
            while ((line = file.ReadLine()) != null)
            {
                _inputs.Add(line);
            }

            file.Close();
        }

        public void Save()
        {
            var file = new System.IO.StreamWriter("output.txt");

            foreach (var output in _outputs)
            {
                file.WriteLine(output);
                Console.WriteLine(output);
            }

            Console.WriteLine("Completed.");

            file.Close();
        }

        public void Start()
        {
            _outputs.Clear();
            for (var k = 0; k < _inputs.Count; k++)
            {
                var result = Calculate(_inputs[k]);
                _outputs.Add($"Case #{k + 1}: {result}");
            }
        }

        protected virtual string Calculate(string input)
        {
            return null;
        }

        public void Start2()
        {
            var outIndex = 0;
            _outputs.Clear();
            for (var k = 0; k < _inputs.Count; k++)
            {
                var result = Calculate2(_inputs[k], _inputs[k + 1]);
                k++;
                _outputs.Add($"Case #{outIndex + 1}: {result}");
                outIndex++;
            }
        }

        protected virtual string Calculate2(string input1, string input2)
        {
            return null;
        }


        protected double GetValueDouble(string input, int index)
        {
            var values = input.Split(' ');
            return Convert.ToDouble(values[index]);
        }

        protected int GetValueInt(string input, int index)
        {
            var values = input.Split(' ');
            return Convert.ToInt32(values[index]);
        }

        protected Int64 GetValueInt64(string input, int index)
        {
            var values = input.Split(' ');
            return Convert.ToInt64(values[index]);
        }

        protected char GetValueChar(string input, int index)
        {
            return input[index];
        }
    }
}
